import firebase from "firebase/app";

import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBY9MMM7WqoLaEqibqy_qrzGEYndOA0MXQ",
    authDomain: "devcamp-reactjs.firebaseapp.com",
    projectId: "devcamp-reactjs",
    storageBucket: "devcamp-reactjs.appspot.com",
    messagingSenderId: "1063611764885",
    appId: "1:1063611764885:web:ad1f302f558255dcf0cb9c"
}

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();
