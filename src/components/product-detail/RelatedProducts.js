import ProductCard from "../products/ProductCard";
import { Grid, Typography } from '@mui/material';
import "../../App.css";
//--------------------

const RelatedProducts = ({ relatedProductData }) => {
    
    return (
        <Grid container spacing={3} marginTop={3}>
            <Grid item xs={12}>
                <Typography variant='h5' fontWeight="bold" className="s24-cl1">Related Products</Typography>
            </Grid>
            <Grid item xs={12}>
                <Grid container spacing={3}>
                    <ProductCard productsData={relatedProductData.slice(0, 6)} />
                </Grid>
            </Grid>
        </Grid>
    )
}
export default RelatedProducts;