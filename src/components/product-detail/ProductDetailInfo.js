import React from "react";
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";

import { Box, Grid, Rating, Stack, Typography, Button } from '@mui/material';
import { styled } from '@mui/material/styles';

import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useSnackbar } from 'notistack';
import "../../App.css";
//--------------------
const ProductImgStyle = styled('img')({
    top: 0,
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    position: 'absolute'
});

const ProductDetailInfo = ({ productDetailData, typeName }) => {
    const dispatch = useDispatch();

    const { productCartsArr } = useSelector((reduxData) => reduxData.taskReducer);

    //snack bar
    const { enqueueSnackbar } = useSnackbar();

    //hàm xử lý add sản phẩm vào giỏ hàng
    const [quantity, setQuantity] = useState(1)
    const onChangeQuantityHandle = (value) => {
        if (value < 1) {
            enqueueSnackbar('Số lượng sản phẩm phải lớn hơn 0!', { variant: "error" });
        }
        else {
            setQuantity(value);
        }
    }
    const onClickAddToCartHandle = () => {
        dispatch({
            type: "ADD_CART",
            payload: {
                product: {
                    productId: productDetailData._id,
                    productName: productDetailData.name,
                    productImg: productDetailData.imageUrl,
                    productPrice: productDetailData.promotionPrice,
                    productQuantity: parseInt(quantity)
                }
            }
        });
    }
    useEffect(() => {
    }, [productCartsArr])
    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={4}>
                <Box sx={{ pt: '100%', position: 'relative' }}>
                    <ProductImgStyle alt="product no." src={require("../../assets/images/Products/" + productDetailData.imageUrl)} />
                </Box>
            </Grid>
            <Grid item xs={12} sm={8}>
                <Stack spacing={2}>
                    <Typography variant='h3' fontWeight="bold" className="s24-cl5">{productDetailData.name}</Typography>
                    <Typography variant='body2' className="s24-cl4">Categories: {typeName}</Typography>
                    <Rating readOnly value={5} />
                    <p className="s24-cl4">{productDetailData.description}</p>
                    <Typography variant="h4" color="red">
                        <Typography component="span" variant="h6" sx={{ color: 'text.disabled', textDecoration: 'line-through' }} >
                            {parseInt(productDetailData.buyPrice).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                        </Typography>
                        &nbsp;
                        {parseInt(productDetailData.promotionPrice).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                    </Typography>
                    <Box sx={{ display: 'flex', alignItems: 'center' }} >
                        <input min={1} type="number" value={quantity} className="form-control" style={{ width: "100px" }} onChange={(event) => onChangeQuantityHandle(event.target.value)} /> &emsp;
                        <Button variant="contained" size='large' endIcon={<ShoppingCartIcon />} sx={{ width: 200 }} id="s24-btn" onClick={onClickAddToCartHandle}>
                            ADD TO CART
                        </Button>
                    </Box>
                </Stack>
            </Grid>
        </Grid>
    )
}
export default ProductDetailInfo;