import { Box, Grid, Stack, Typography, Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import "../../App.css";

//--------------------
const ProductImgStyle = styled('img')({
    top: 0,
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    position: 'absolute'
});

const ProductDetailDescription = ({ productDetailData }) => {

    return (
        <Grid container spacing={3} marginTop={3}>
            <Grid item xs={12}>
                <Stack spacing={2}>
                        <Typography variant='h5' fontWeight="bold" className='s24-cl1'>Description</Typography>
                        <p className='s24-cl4'>{productDetailData.description}</p>
                        <Box sx={{ pt: '100%', position: 'relative' }}>
                            <ProductImgStyle alt="product no." src={require("../../assets/images/Products/" + productDetailData.imageUrl)} />
                        </Box>
                </Stack>
            </Grid>
            <Grid item xs={12} textAlign="center">
                <Button variant="contained" size='large' id='s24-btn'>
                    View All
                </Button>
            </Grid>
        </Grid>
    )
}
export default ProductDetailDescription;