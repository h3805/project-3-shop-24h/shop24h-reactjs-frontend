import { NativeSelect } from "@mui/material";
import { useState } from "react";
import "../App.css";

const SelectShowProductNumber = ({ showPage, selectShowPageHandleProp }) => {
    const [page, setPage] = useState(showPage);
    const handleChange = (event) => {
        setPage(event.target.value);
        selectShowPageHandleProp(event.target.value);
    };
    return (
        <>
            <span className="s24-cl1">Show &nbsp; <NativeSelect value={page} onChange={handleChange} sx={{ border: "none" }}>
                <option value={9}>9</option>
                <option value={15}>15</option>
                <option value={30}>30</option>
            </NativeSelect>products</span>
        </>
    )
}

export default SelectShowProductNumber;
