import { Box, Card, Grid, Typography, Stack } from "@mui/material";
import { styled } from '@mui/material/styles';
import { Link } from "react-router-dom";
import "../../App.css";

const ProductImgStyle = styled('img')({
    top: 0,
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    position: 'absolute'
});
const ProductCard = ({ productsData }) => {
    return (
        <>
            {productsData ?
                productsData.map((product, index) => {
                    return (
                        <Grid item xs={12} sm={6} md={4} key={index}>
                            <Card>
                                <Box sx={{ pt: '100%', position: 'relative' }}>
                                    <Link to={"/products/" + product._id} >
                                        <ProductImgStyle alt="product no." src={require("../../assets/images/Products/" + product.imageUrl)} />
                                    </Link>
                                </Box>
                                <Stack spacing={2} sx={{ p: 3 }} textAlign="center" className="s24-bg3">
                                    <Link to={"/products/" + product._id} style={{textDecorationLine: "none" }} className="s24-cl1">
                                        <Typography variant="body1" fontWeight="bold" noWrap>
                                            {product.name}
                                        </Typography>
                                    </Link>
                                    <Stack direction="row" justifyContent="center">
                                        <Typography variant="body1" className="s24-cl5">
                                            <Typography component="span" variant="body2" sx={{ color: 'text.disabled', textDecoration: 'line-through' }} >
                                                {parseInt(product.buyPrice).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                                            </Typography>
                                            &nbsp;
                                            {parseInt(product.promotionPrice).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                                        </Typography>
                                    </Stack>
                                </Stack>
                            </Card>
                        </Grid>
                    )
                })
                :
                <>
                </>
            }
        </>
    )
}
export default ProductCard;