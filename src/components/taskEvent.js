const initialState = {
  productCartsArr: localStorage.getItem("shop24hProductCartsArr") ? JSON.parse(localStorage.getItem("shop24hProductCartsArr")) : [],
  total: localStorage.getItem("shop24hCartTotal") ? parseInt(localStorage.getItem("shop24hCartTotal")) : 0,
  userCus: {
    fullName: "none",
    email: "none"
  },
  userId: localStorage.getItem("shop24hUserId") ? JSON.parse(localStorage.getItem("shop24hUserId")) : "0"
};

const taskEvent = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_CART": {
      let flag = true;
      state.productCartsArr.forEach((element, index) => {
        if (element.productId === action.payload.product.productId) {
          element.productQuantity = element.productQuantity + action.payload.product.productQuantity;
          flag = false;
        }
      });
      if (flag) {
        state.productCartsArr = [...state.productCartsArr, action.payload.product];
      }
      localStorage.setItem("shop24hProductCartsArr", JSON.stringify(state.productCartsArr));
      localStorage.setItem("shop24hCartTotal", state.total + (action.payload.product.productQuantity * action.payload.product.productPrice));
      return {
        ...state,
        productCartsArr: state.productCartsArr,
        total: state.total + (action.payload.product.productQuantity * action.payload.product.productPrice)
      };
    }
    case "DELETE_ITEM_CART": {
      let newArr = state.productCartsArr.filter(ele => {
        return (
          ele.productId !== action.payload.product.productId
        )
      })
      localStorage.setItem("shop24hProductCartsArr", JSON.stringify(newArr));
      localStorage.setItem("shop24hCartTotal", state.total - (action.payload.product.productQuantity * action.payload.product.productPrice));
      return {
        ...state,
        productCartsArr: newArr,
        total: state.total - (action.payload.product.productQuantity * action.payload.product.productPrice)
      };
    }
    case "UPDATE_ITEM_CART": {
      let totalItemOld = 0;
      let totalItemNew = 0;
      state.productCartsArr.forEach((ele, index) => {
        if (ele.productId === action.payload.product.productId) {
          totalItemOld = ele.productQuantity * ele.productPrice
          ele.productQuantity = action.payload.product.productQuantity
        }
      })
      if (action.payload.product.totalAdd > 0) {
        totalItemNew = action.payload.product.totalAdd
      }
      localStorage.setItem("shop24hProductCartsArr", JSON.stringify(state.productCartsArr));
      localStorage.setItem("shop24hCartTotal", state.total - totalItemOld + totalItemNew);
      return {
        ...state,
        productCartsArr: state.productCartsArr,
        total: state.total - totalItemOld + totalItemNew
      };
    }
    case "ADD_USER": {
      return {
        ...state,
        userCus: action.payload.userCus
      };
    }
    case "RESET_CART": {
      localStorage.removeItem("shop24hProductCartsArr");
      localStorage.removeItem("shop24hCartTotal");
      return {
        ...state,
        productCartsArr: [],
        total: 0
      };
    }
    case "ADD_USER_ID": {
      localStorage.setItem("shop24hUserId", JSON.stringify(action.payload.userId));
      return {
        ...state,
        userId: action.payload.userId
      };
    }
    default: {
      return state;
    }
  }
};

export default taskEvent;
