import CarouselContent from "./Carousel";
import LastestProducts from "./LastestProducts";
import { useEffect, useState } from 'react';
import ViewAll from "./ViewAll";
import "../../App.css";

const Content = () => {
    const [productData, setProductData] = useState([]);
    const [url, setUrl] = useState("http://localhost:8000/products?limit=8");
    const getProduct = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }

    //Hàm xử lý sự kiện nút viewAll: thay đổi chữ, thay đổi url để hiển thị dữ liệu
    const [viewAll, setViewAll] = useState(false);
    const onClickButtonViewHandle = () => {
        if (viewAll === false) {
            setViewAll(true);
            setUrl("http://localhost:8000/products");
        } else {
            setViewAll(false);
            setUrl("http://localhost:8000/products?limit=8");
        }
    }

    useEffect(() => {
        getProduct(url)
            .then((data) => {
                setProductData(data.data);
            })
            .catch((error) => {
                console.log(error);
            });
    }, [url, viewAll])
    return (
        <div className="mt-5 s24-backgroundPage">
            <CarouselContent />
            <LastestProducts productsData={productData} />
            <ViewAll buttonContent={viewAll} buttonClick={onClickButtonViewHandle} />
        </div>
    )
}
export default Content;