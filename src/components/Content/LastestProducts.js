import { Container, Box, Card, Grid, Typography, Stack } from "@mui/material";
import { styled } from '@mui/material/styles';
import { Link } from "react-router-dom";
import "../../App.css";

const ProductImgStyle = styled('img')({
    top: 0,
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    position: 'absolute'
});
const LastestProducts = ({ productsData }) => {
    return (
        <Container sx={{ marginY: 0, paddingTop: 10 }} className="s24-backgroundPage">
            <Typography className="s24-cl1" variant="h5" textAlign="center" sx={{ fontWeight: 'bold' }}>
                LASTEST PRODUCTS
            </Typography>
            <Grid container spacing={3} marginY={3}>
                {productsData ?
                    productsData.map((product, index) => {
                        return (
                            <Grid item xs={12} sm={4} md={3} key={index}>
                                <Card className="s24-bg3">
                                    <Box sx={{ pt: '100%', position: 'relative' }}>
                                        <Link to={"/products/" + product._id}>
                                            <ProductImgStyle alt="product no." src={require("../../assets/images/Products/" + product.imageUrl)} />
                                        </Link>
                                    </Box>
                                    <Stack spacing={2} sx={{ p: 3 }} textAlign="center">
                                        <Link to={"/products/" + product._id} style={{ textDecorationLine: "none" }} className="s24-cl1">
                                            <Typography variant="body1" fontWeight="bold" noWrap>
                                                {product.name}
                                            </Typography>
                                        </Link>
                                        <Stack direction="row" justifyContent="center">
                                            <Typography variant="body1" className="s24-cl5">
                                                <Typography component="span" variant="body2" sx={{ color: 'text.disabled', textDecoration: 'line-through' }} >
                                                    {parseInt(product.buyPrice).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                                                </Typography>
                                                &nbsp;
                                                {parseInt(product.promotionPrice).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                                            </Typography>
                                        </Stack>
                                    </Stack>
                                </Card>
                            </Grid>
                        )
                    })
                    :
                    <>
                    </>
                }
            </Grid>
        </Container>
    )
}
export default LastestProducts;