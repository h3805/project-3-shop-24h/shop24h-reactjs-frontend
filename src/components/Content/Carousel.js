import { Carousel } from "react-bootstrap"
import Carousel1 from "../../assets/images/Slide/Carousel1.jpg"
import Carousel2 from "../../assets/images/Slide/Carousel2.jpg"
import Carousel3 from "../../assets/images/Slide/Carousel3.jpg"
import Carousel4 from "../../assets/images/Slide/Carousel4.jpg"
import Carousel5 from "../../assets/images/Slide/Carousel5.jpg"
import Carousel6 from "../../assets/images/Slide/Carousel6.jpg"

const CarouselContent = () => {
  return (
    <Carousel>
      <Carousel.Item>
        <img className="d-block w-100"  src={Carousel1} alt="Carousel1" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100"  src={Carousel2} alt="Carousel2" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={Carousel3} alt="Carousel3" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={Carousel4} alt="Carousel4" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={Carousel5} alt="Carousel5" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={Carousel6} alt="Carousel6" />
      </Carousel.Item>
    </Carousel>
  )
};

export default CarouselContent;