import 'bootstrap/dist/css/bootstrap.min.css';
import "../../App.css";

import { Fragment, useState } from 'react';
import { Box, Collapse, IconButton, Table, TableBody, TableCell, TableHead, TableRow, TextareaAutosize, Typography } from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

const RowCollapse = (props) => {
    const { orderRow } = props;
    const [openCollapse, setopenCollapsese] = useState(false);

    return (
        <Fragment>
            <TableRow>
                <TableCell style={{ borderBottom: 'none' }}>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setopenCollapsese(!openCollapse)}
                    >
                        {openCollapse ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row" align="left" style={{ borderBottom: 'none' }}>
                    {orderRow._id}
                </TableCell>
                <TableCell align="center" style={{ borderBottom: 'none' }}>
                    {orderRow.orderDate ? orderRow.orderDate.slice(0, 10) : ""}
                </TableCell>
                <TableCell align="center" style={{ borderBottom: 'none' }}>
                    {orderRow.shippedDate ? orderRow.shippedDate.slice(0, 10) : ""}
                </TableCell>
                <TableCell align="center" style={{ borderBottom: 'none' }}>
                    {orderRow.cost ? orderRow.cost.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' }) : ""}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 1, paddingTop: 1 }} colSpan={5}>
                    <Collapse in={openCollapse} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography variant="body1" gutterBottom component="div" sx={{ fontWeight: "bold" }}>
                                PRODUCTS LIST
                            </Typography>
                            <Table size="small" aria-label="producList">
                                <TableHead>
                                    <TableRow>
                                        <TableCell colSpan={2} align="center"><b>Product name</b></TableCell>
                                        <TableCell align="center"><b>Price</b></TableCell>
                                        <TableCell align="center"><b>Quantity</b></TableCell>
                                        <TableCell align="center"><b>Subtotal</b></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        orderRow.orderDetail ?
                                            orderRow.orderDetail.map((productRow, indexProduct) => (
                                                <TableRow key={indexProduct}>
                                                    <TableCell align="right">
                                                        <img src={require("../../assets/images/Products/" + productRow.productImg)} alt="product" height="48px" width="48px" />
                                                    </TableCell>
                                                    <TableCell align="left">{productRow.productName}</TableCell>
                                                    <TableCell align="center">{productRow.productPrice.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</TableCell>
                                                    <TableCell align="center">{productRow.productQuantity}</TableCell>
                                                    <TableCell align="center">{(productRow.productPrice * productRow.productQuantity).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</TableCell>
                                                </TableRow>
                                            ))
                                            :
                                            <TableRow>
                                                <TableCell colSpan={5}>No product...</TableCell>
                                            </TableRow>
                                    }
                                    <TableRow>
                                        <TableCell colSpan={5}>
                                            <TextareaAutosize
                                                disabled
                                                aria-label="note"
                                                minRows={2}
                                                placeholder="Note..."
                                                style={{ width: "100%" }}
                                                value={orderRow.note}
                                            />
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </Fragment>
    );
}

export default RowCollapse;