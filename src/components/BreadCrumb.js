import { Breadcrumbs, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import "../App.css";

const BreadCrumb = ({ breadCrumbList }) => {

    return (
        <>
            <Breadcrumbs aria-label="breadcrumb" sx={{ fontSize: 20 }}>
                {
                    breadCrumbList.map((ele, index) => {
                        if (ele.url === "#") {
                            return (
                                <Link key={index} style={{ textDecorationLine: "unset" }} to={ele.url}>
                                    <Typography className="s24-cl1 fw-bold">
                                        {ele.name}
                                    </Typography>
                                </Link>
                            )
                        }
                        else {
                            return (
                                <Link key={index} className="s24-link" to={ele.url}>
                                    <Typography className="text-muted fw-bold">
                                        {ele.name}
                                    </Typography>
                                </Link>
                            )
                        }
                    })
                }
            </Breadcrumbs>
        </>
    )
}
export default BreadCrumb;