import { Grid, Typography } from "@mui/material";

const InfoFooter1 = () => {
    return (
        <>
            <Grid item xs={4}>
                <Typography sx={{ fontWeight: 'bold' }} marginY={2} noWrap>
                    PRODUCTS
                </Typography>
                <Typography marginY={1} noWrap>
                    Help Center
                </Typography>
                <Typography marginY={1} noWrap>
                    Contact Us
                </Typography>
                <Typography marginY={1} noWrap>
                    Product Help
                </Typography>
                <Typography marginY={1} noWrap>
                    Warranty
                </Typography>
                <Typography marginY={1} noWrap>
                    Order Status
                </Typography>
            </Grid>
        </>
    )
}
export default InfoFooter1;