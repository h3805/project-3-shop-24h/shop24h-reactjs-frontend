import { Grid, Typography } from "@mui/material";
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TwitterIcon from '@mui/icons-material/Twitter';

const SocialFooter = () => {
    return (
        <>
            <Grid container textAlign="center" marginTop={3}>
                <Grid item xs={12} padding={1}>
                    <Typography variant="h4" sx={{ fontWeight: 'bold' }}>
                        DevCamp
                    </Typography>
                </Grid>
                <Grid item xs={12} padding={1}>
                    <FacebookIcon fontSize="large" />&nbsp;
                    <InstagramIcon fontSize="large" />&nbsp;
                    <YouTubeIcon fontSize="large" />&nbsp;
                    <TwitterIcon fontSize="large" />
                </Grid>
            </Grid>
        </>
    )
}
export default SocialFooter;