const ProductTypesData = [
    {
        "name": "Television and Video",
        "description": "TV, Photo, Video, UAV, Accessories for TV"
    },
    {
        "name": "Smartphones",
        "description": "Smartphones and Accessories for phones"
    },
    {
        "name": "Life Style",
        "description": "Health Care, Clothing, Shoes, Backpacks & Bags, Bedding & Bath, Kitchen & Dining"
    },
    {
        "name": "Smart Device",
        "description": "Sockets & Sensors, Smart Lighting, WiFi Routers, Air & Water Purifiers, Beauty & Personal CarePower Strips, Cleaning Gear"
    },
    {
        "name": "Tablets Laptops",
        "description": "Notebooks, Mi Tablets, Accessories for laptops, Accessories for tablets"
    },
    {
        "name": "Power Supplies",
        "description": "Power Banks, Batteries & Battery Packs, Accessories for banks, Wireless Chargers"
    },
    {
        "name": "Audio",
        "description": "Headphones, Earphones, Speakers, Accessories for audio"
    },
    {
        "name": "Watches",
        "description": "Amazfit Trackers, Mi Band, Smart Watches, Accessories"
    },
    {
        "name": "Personal transport",
        "description": "Electric Scooters, Electric Bikes, Accessories for transport"
    }
]

//------------------------------------------------
const ProductsData = [
    {
        "name": "Mi TV Q1",
        "description": "Mi TV Q1 75inch Dynamic local dimming creates stunning dark and bright tones",
        "type": "Television and Video",
        "imageUrl": "miTvQ1.jpg",
        "buyPrice": "36000000",
        "promotionPrice": "29999999"
    },
    {
        "name": "Mi LED TV 4",
        "description": "55 panel of 4K resolution that runs Android TV OS with Xiaomi's in-house Patchwall - a deep learning AI system",
        "type": "Television and Video",
        "imageUrl": "MiLEDTV4.jpg",
        "buyPrice": "30000000",
        "promotionPrice": "25999999"
    },
    {
        "name": "Xiaomi 12 Pro",
        "description": "Pro-grade triple 50MP camera array. Leading Snapdragon® 8 Gen 1. WQHD+ dynamic 120Hz AMOLED display. Smart 120W Xiaomi HyperCharge",
        "type": "Smartphones",
        "imageUrl": "xiaomi12Pro.jpg",
        "buyPrice": "19300000",
        "promotionPrice": "15999999"
    },
    {
        "name": "Xiaomi 11",
        "description": "108MP pro-grade camera. 120W Xiaomi HyperCharge. 120Hz AMOLED, Dolby Vision® supported · Flagship Qualcomm® Snapdragon™ 888",
        "type": "Smartphones",
        "imageUrl": "xiaomi11.jpg",
        "buyPrice": "14000000",
        "promotionPrice": "11999999"
    },
    {
        "name": "Mi Air Purifier 4",
        "description": "Breathe clean, breathe healthy20m² large room purification in approx. 10 minutes. Smart control. OLED display",
        "type": "Smart Device",
        "imageUrl": "MiAirPurifier4.jpg",
        "buyPrice": "6190000",
        "promotionPrice": "4390000"
    },
    {
        "name": "Mi Smart Water Purifier",
        "description": "Original Xiaomi Mi Smart Water Purifier 600G Under Life Style Reverse Osmosis Water Intelligent Monitor Water Purifier",
        "type": "Smart Device",
        "imageUrl": "MiSmartWaterPurifier.jpg",
        "buyPrice": "750000",
        "promotionPrice": "645000"
    },
    {
        "name": "MIJIA Mini Electric Rice Cooker",
        "description": "XIAOMI MIJIA Mini Electric Rice Cooker Intelligent Automatic household Life Style Cooker 1-2 people small electric rice cookers",
        "type": "Life Style",
        "imageUrl": "MIJIAMiniRiceCooker.jpg",
        "buyPrice": "2190000",
        "promotionPrice": "1999999"
    },
    {
        "name": "MIJIA Electric Kettle",
        "description": "Xiaomi Planet - Xiaomi Mi Kettle 1S, is a new kettle with a nice look and a great price",
        "type": "Life Style",
        "imageUrl": "MIJIAElectricKettle.jpg",
        "buyPrice": "865000",
        "promotionPrice": "550000"
    },
    {
        "name": "Xiaomi Vacuum Mop SKV4093GL",
        "description": "The Xiaomi Mi Robot Vacuum is a mid-range robot vacuum. It feels well-built, has a long battery life, and maneuvers itself effectively.",
        "type": "Smart Device",
        "imageUrl": "XiaomiVacuumMopSKV4093GLL.jpg",
        "buyPrice": "6190000",
        "promotionPrice": "5190000"
    },
    {
        "name": "Mijia Projector Youth Edition 2",
        "description": "Mi Smart Compact Projector. A mini high-definition cinema around you. 1080P resolution | Average 500 ANSI lumens | Totally sealed optical system | Large integrated sound chamber",
        "type": "Television and Video",
        "imageUrl": "MijiaProjectorYouthEdition2.jpg",
        "buyPrice": "19000000",
        "promotionPrice": "16999999"
    },
    {
        "name": "Mijia Induction Cooker Youth 2100W",
        "description": "Cheap Induction Cookers, Buy Quality Home Appliances Directly from China Suppliers:Xiaomi Mijia induction cookers Mi home Smart electric tile oven",
        "type": "Life Style",
        "imageUrl": "MijiaInductionCooker.jpg",
        "buyPrice": "1100000",
        "promotionPrice": "980000"
    },
    {
        "name": "MIJIA Air Conditioner Premium Edition",
        "description": "Xiaomi launches Gentle Breeze air conditioner: Energy saving, voice control",
        "type": "Smart Device",
        "imageUrl": "MIJIAAirConditioner.jpg",
        "buyPrice": "12640000",
        "promotionPrice": "10999999"
    },
    {
        "name": "Mijia Food Blender Cooking Machine",
        "description": "Xiaomi Mijia Food Blender Cooking Machine Mixer Food Maker Portable Electric Fruit Juicer Squeezer Household Travel Extractor Kitchen Tools",
        "type": "Life Style",
        "imageUrl": "MijiaFoodBlenderCooking.jpg",
        "buyPrice": "6500000",
        "promotionPrice": "4900000"
    },
    {
        "name": "Mijia Wireless Vacuum Cleaner",
        "description": "Xiaomi Mijia Wireless Vacuum Cleaner Lite For Home Car Handheld 17KPa Strong Sunction 45min Endurance Light Weight Triple Filtration SystemWhiteMulti Brush Dust Catcher(MJWXCQ03DY)",
        "type": "Smart Device",
        "imageUrl": "MijiaWirelessVacuumCleaner.jpg",
        "buyPrice": "5500000",
        "promotionPrice": "3650000"
    },
    {
        "name": "Xiaomi Watch 2 Lite",
        "description": "1.55' colourful touch display. 100+ fitness modes.  5 ATM water resistance. SpO₂ measurement* and 24-hour heart rate tracking...",
        "type": "Watches",
        "imageUrl": "XiaomiWatch2Lite.jpg",
        "buyPrice": "1290000",
        "promotionPrice": "990000"
    },
    {
        "name": "Xiaomi Smart Home Security",
        "description": "Xiaomi Mi Smart Sensor Set 5 in 1 contains a base, two motion sensors, an intelligent switch, but also two pieces of sensors for placement on a window or door",
        "type": "Smart Device",
        "imageUrl": "XiaomiSmartHomeSecurity.jpg",
        "buyPrice": "500000",
        "promotionPrice": "455000"
    },
    {
        "name": "Mi Electric Scooter 3",
        "description": "Strong performance to help with riding. 30km extra long-range. Three speed modes, easy switching...",
        "type": "Personal transport",
        "imageUrl": "MiElectricScooter3.jpg",
        "buyPrice": "13500000",
        "promotionPrice": "10999999"
    },
    {
        "name": "Mijia Oral Irrigator",
        "description": "Cheap Smart Remote Control, Buy Quality Consumer Electronics Directly from China Suppliers:Xiaomi Mijia Electric Oral Irrigator Water Flosser 200ml ...",
        "type": "Smart Device",
        "imageUrl": "MijiaOralIrrigator.jpg",
        "buyPrice": "900000",
        "promotionPrice": "739000"
    },
    {
        "name": "Mijia LED Desk Lamp",
        "description": "Cheap Desk Lamps, Buy Quality Lights & Lighting Directly from China Suppliers:Xiaomi Mijia LED Desk Lamp Lite For Study Work Brightness Adajustable Mi",
        "type": "Smart Device",
        "imageUrl": "MijiaLEDDeskLamp.jpg",
        "buyPrice": "780000",
        "promotionPrice": "610000"
    },
    {
        "name": "Mijia Bluetooth Thermometer 2",
        "description": "Home hygrometer, data can be clearly read in various brightness and angle. High sensitivity and accurate sensirion. Via Mi app to switch on humidifier...",
        "type": "Smart Device",
        "imageUrl": "MijiaBluetoothThermometer2.jpg",
        "buyPrice": "130000",
        "promotionPrice": "105000"
    },
    {
        "name": "Xiaomi Pad 5",
        "description": "Xiaomi Pad 5 is the new tablet on the market from the Mi brand that has broken all sales records! This tablet which is based on Android 11 can be called a multitasking device, as it has a plenty functions. The main gadget advantage is its high performance, provided by the Qualcomm SnapdragonTM 860 processor. It allows you to enjoy watching videos, playing games, or just surfing the Internet. The gadget has a capacious battery of 8720 mAh, which is enough for more than 5 days of device life (depending on the frequency of use). Designers will love the tablet as it is equipped with a smart Xiaomi pen that allows you to write or draw on the screen.",
        "type": "Tablets Laptops",
        "imageUrl": "XiaomiPad5.jpg",
        "buyPrice": "9650000",
        "promotionPrice": "8990000"
    },
    {
        "name": "RedmiBook Pro 15",
        "description": "The powerful RedmiBook Pro 15 delivers both great leisure time and productive work. The device is designed for people who value ergonomics. This laptop weighs less than two kilograms, remaining a highly effective device that will handle even the most resource-intensive programs and games. Among other advantages of this model is the graphics adapter NVIDIA GeForce MX450, which allows you to enjoy the high-quality picture of movies and video games. In addition, the device has a resolution of 3200x2000 pixels, which makes you dive deeper into this virtual world",
        "type": "Tablets Laptops",
        "imageUrl": "RedmiBookPro15.jpg",
        "buyPrice": "24500000",
        "promotionPrice": "21990000"
    },
    {
        "name": "Redmi G gaming laptop",
        "description": "Having conquered the mobile market, Xiaomi began to conquer the hearts of gamers by launching gaming laptops with the latest generation of processors. That is a model that will delight players with a high-quality screen with a resolution of 1920 x 1080 pixels, which conveys clarity and colors as close to real as possible. The Intel Core i5-10200H processor with four cores and NVIDIA graphics provide dignified performance that will help you not only to enjoy the game but also to solve many work tasks. The laptop has a powerful cooling system for the processor, which will extend the duration of its life. This laptop can be used for work since, despite all the functionality, it still retains portability.",
        "type": "Tablets Laptops",
        "imageUrl": "RedmiGGamingLaptop.jpg",
        "buyPrice": "32500000",
        "promotionPrice": "28900000"
    },
    {
        "name": "Xiaomi Mi Power Bank 3",
        "description": "Mi Power bank 3 with a battery capacity of 10,000 mAh, can quickly charge your devices. Microcurrent technology and multiple smartphone charging The use of microcurrent technology will allow you to charge fitness trackers and Bluetooth headsets. 3 outputs for convenient work (USB Type-C, Micro-USB and USB-A) Aluminum case Mi Power bank 3 10,000 mAh has an aluminum body with a special coating. So it is quite resistant to scratches. 9-level protection",
        "type": "Power Supplies",
        "imageUrl": "XiaomiMiPowerBank3.jpg",
        "buyPrice": "350000",
        "promotionPrice": "320000"
    },
    {
        "name": "Xiaomi Mi Power Bank 10000mAh Red",
        "description": "First-class lithium-ion battery pack from LG and Samsung adds 10,000 mAh duration of the built-in default battery on your smartphone, tablet or digital camera. Powerful Power Bank is able to add up to 2 times longer operating hours, talking about the smartphone Mi 3.",
        "type": "Power Supplies",
        "imageUrl": "XiaomiMiPowerBank10000mAhRed.jpg",
        "buyPrice": "200000",
        "promotionPrice": "140000"
    },
    {
        "name": "Xiaomi Rainbow 7 AAA Batteries",
        "description": "Once you see battery Mi Rainbow, you will immediately like them! 10 different bright colors that, to some extent, enliven batteries. Now you can easily distinguish old from the new battery, and also choose your favorite color of batteries for your home devices. Japanese company Hitachi Maxell, which is a manufacturer of Mi Rainbow, will provide you with reliable, safe and long lasting batteries. Updated engineering, both internal and external parts of the battery. Mi Rainbow — is example of manifestation of love for life",
        "type": "Power Supplies",
        "imageUrl": "XiaomiRainbow7AAABatteries.jpg",
        "buyPrice": "75000",
        "promotionPrice": "65000"
    },
    {
        "name": "Mi Wireless Car Charger",
        "description": "Mi 20W Wireless Car Charger. Electric adjustable grip, 20W high-power flash charging. Electrically adjusts the grip when your drive starts. Automatically grips and begins charging as soon as it touches your phone ",
        "type": "Power Supplies",
        "imageUrl": "MiWirelessCarCharger.jpg",
        "buyPrice": "830000",
        "promotionPrice": "590000"
    },
    {
        "name": "Xiaomi FlipBuds Pro",
        "description": "Xiaomi FlipBuds Pro is the new Mi brand wireless earbuds that will delight you with sound quality and long battery life. Headphones are sure to please almost every user since their design was developed extremely scrupulously. The device is made in an ergonomic shape - before the creators came to the final version, the shape was corrected more than two dozen times! One of the gadget's main features is the effective noise reduction system, provided thanks to the built-in Qualcomm QCC515 chip. The headset will be able to delight you with high-quality sound for a long time because the duration of its operation reaches 28 hours when using the charging case.",
        "type": "Audio",
        "imageUrl": "XiaomiFlipBudsPro.jpg",
        "buyPrice": "2200000",
        "promotionPrice": "1690000"
    },
    {
        "name": "Mi True Wireless Earphones 2S",
        "description": "The Mi True Wireless Earphones 2S headset is a compact and comfortable solution for everyone who appreciates high-quality sound. The earbuds have an ergonomic design, and their material fits snugly to your ear without creating unpleasant pressure. The device has an effective noise cancellation system, which will allow you to use them in noisy environments for talking on the phone. The case with its full charge can charge the headset about four times, and the life of the headset itself consists of about 5 hours.",
        "type": "Audio",
        "imageUrl": "MiTrueWirelessEarphones2S.jpg",
        "buyPrice": "800000",
        "promotionPrice": "600000"
    },
    {
        "name": "Xiaomi XiaoAI Art Speaker",
        "description": "Hear the symphony of a perfect sound. The speaker's sound effects are provided with 10531 holes. There is a touch panel on the top where the user can set up the volume, turn on/off the sound and microphone. The speaker can be operated by voice control (XiaoAI is supported) The speaker has a built-in backlight which supports 16 million colors. A beautiful gradient glow will sparkle while the music is playing. The sound settings allow adjusting the sound effect like vocal, bass etc.",
        "type": "Audio",
        "imageUrl": "XiaomiXiaoAIArtSpeaker.jpg",
        "buyPrice": "4500000",
        "promotionPrice": "3999000"
    },
    {
        "name": "Mi Band 6",
        "description": "The fitness bracelet Xiaomi Mi Band 6 with NFC support is the innovation that everyone has been waiting for. This gadget differs from the previous models with an improved display. The 1.56-inch screen makes it much easier to see all the information you need. Besides, it will help to improve the notification functions, as they will be better visible. The bracelet has an extended range of dials to meet the needs of any user. With Mi Band 6, you don't have to download extraneous apps to choose an authentic screensaver design. Also, the new model has a pulse oximeter function, which will be more useful than ever in a pandemic. Traditionally, the straps are made of high-quality hypoallergenic materials.",
        "type": "Watches",
        "imageUrl": "MiBand6.jpg",
        "buyPrice": "1100000",
        "promotionPrice": "890000"
    },
    {
        "name": "Segway Self Balancing Scooter",
        "description": "18 km reach. Riding your miniLITE is easy and fun! Indoors or outdoors, go anywhere you like and cover distances up to 18 km. 16 km/h speed. Take the Segway miniLITE for a ride and speed up to 16 km/h (10 mph). The Ninebot by Segway App helps you to learn quickly how to master your miniLITE. 12.5 kg weight The Segway miniLITE’s knee-control bar doubles as a handle that allows for easy lifting. Take the miniLITE with you anywhere you go.",
        "type": "Personal transport",
        "imageUrl": "SegwaySelfBalancingScooter.jpg",
        "buyPrice": "19000000",
        "promotionPrice": "15990000"
    },
    {
        "name": "Mijia Women`s Smart Shoes",
        "description": "Mi Home (Mijia) Women`s Smart Shoes White/Pink Size 35-41",
        "type": "Life Style",
        "imageUrl": "MijiaWomenSmartShoes.jpg",
        "buyPrice": "1200000",
        "promotionPrice": "890000"
    },
    {
        "name": "Mijia Men`s Smart Shoes",
        "description": "Mi Home (Mijia) Men`s Smart Shoes White/Gray Size 39-47",
        "type": "Life Style",
        "imageUrl": "MijiaMenSmartShoes.jpg",
        "buyPrice": "1200000",
        "promotionPrice": "890000"
    },
    {
        "name": "Xiaomi Mi Minimalist Urban Backpack",
        "description": "Keep your hands free during your day with this Laptop Backpack. Refined style adds a touch of uniqueness to your style. The adjustable shoulder straps make this Laptop Backpack comfortable to wear even when loaded with all your belongings. Ideal for everyday wear, this Laptop Backpack is styled with adjustable handles and great closure",
        "type": "Life Style",
        "imageUrl": "XiaomiMiMinimalistUrbanBackpack.jpg",
        "buyPrice": "650000",
        "promotionPrice": "550000"
    },
    {
        "name": "Xiaomi Mi Casual College Backpack",
        "description": "Xiaomi Mi Casual College Backpack is a great solution for the younger generation. It is tall, slender, and still very spacious. Since today young people like to carry around more and more things around in their backpacks, it puts them at a risk of getting a back pain or other disorders. Xiaomi Mi Casual College Backpack is large enough, however, the company took care of reducing the load on the shoulders and back. Wide straps and a firm breathable back evenly distribute weight and allow to prevent health problems.",
        "type": "Life Style",
        "imageUrl": "XiaomiMiCasualCollegeBackpack.jpg",
        "buyPrice": "400000",
        "promotionPrice": "300000"
    },
    {
        "name": "Xiaomi UREVO Travel Suitcase",
        "description": "The Xiaomi UREVO Travel suitcase will be your best travel friend. The suitcase is made materials that are resistant to dirt and mechanical damage. This is what ensures the durability of the case. The device has an ergonomic handle that can be adjusted to the height of the owner. In addition, the design of the suitcase includes wheels that rotate 360 ​​degrees, which helps to overcome any obstacles and maneuver freely. The case has a lock that reduces the risk of unauthorized access to its contents.",
        "type": "Life Style",
        "imageUrl": "XiaomiUREVOTravelSuitcase.jpg",
        "buyPrice": "2500000",
        "promotionPrice": "1990000"
    },
    {
        "name": "Xiaomi Pao Pao Soap Dispenser",
        "description": "Xiaomi Pao Pao Smart Soap Dispenser is the best high-tech replacement for your home or office soap dish. You do not need to touch the common soap and lather it for a long time anymore - just move your hand near the built-in motion sensor, and a portion of fresh foam will be in your hands. The dispenser passes air through the soapy liquid, significantly reducing the material consumption and increasing the quality of its use. The device can work up to 6 months without changing batteries, after which it notifies you of insufficient charge using a special LED indicator.",
        "type": "Life Style",
        "imageUrl": "XiaomiPaoPaoSoapDispenser.jpg",
        "buyPrice": "500000",
        "promotionPrice": "350000"
    },
    {
        "name": "Hbada Ergonomic Office Chair White",
        "description": "A chair for study or work is the most important attribute in a room. The matter concerns health, namely the correct posture and setting of the spine. The slightest inconvenience can lead to curvature or illness. When choosing a chair, it is important to pay attention to even the smallest details. Hbada ergonomic office chair is the case when all your wishes are taken into account: the chair is as comfortable as possible, with an ergonomic back and comfortable armrests, the material is resistant to any damage. The chair can be moved thanks to the wheels, and the purchase will serve the owner for many years..",
        "type": "Life Style",
        "imageUrl": "HbadaErgonomicOfficeChairWhite.jpg",
        "buyPrice": "1500000",
        "promotionPrice": "1350000"
    },
    {
        "name": "Diiib Floor Drain Set",
        "description": "Diiib Floor Drain Set is a modern model of the floor drain. The product is made of high-quality stainless anti-corrosion steel. Floor drain can be installed in any room of a residential or industrial building. Among the main features of the drain: • Insect protection • Versatility of use • Reliability and durability • Stylish ergonomic design solution • Ability to remotely control • Dimensions: 100 * 100 * 62 mm • Product weight: 242 grams",
        "type": "Life Style",
        "imageUrl": "DiiibFloorDrainSet.jpg",
        "buyPrice": "800000",
        "promotionPrice": "600000"
    }
]