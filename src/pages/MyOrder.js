import 'bootstrap/dist/css/bootstrap.min.css';
import "../App.css";
import BreadCrumb from '../components/BreadCrumb';
import Footer from '../components/Footer/Footer';
import Header from '../components/Header/Header';
import RowCollapse from '../components/MyOrder/RowCollapse';

import { useSelector } from "react-redux";
import { useEffect, useState } from 'react';
import { useSnackbar } from 'notistack';
import { Container, Grid, Pagination, InputBase, IconButton, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from '@mui/material';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';


const MyOrder = () => {
    const { userId } = useSelector((reduxData) => reduxData.taskReducer);
    const { enqueueSnackbar } = useSnackbar();
    //khai báo mảng breadcrumb
    const breadCrumbList = [{
        name: "Home",
        url: "/"
    }, {
        name: "My Order",
        url: "#"
    }]

    //khai báo biến
    const [fullName, setFullName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [country, setCountry] = useState("");
    const [orderList, setOrderList] = useState([])

    //Search
    const [inputOrderIdSearch, setInputOrderIdSearch] = useState("");
    const [condition, setCondition] = useState("");
    const onBtnSearchClickHandle = () => {
        if (inputOrderIdSearch.match(/^[0-9a-fA-F]{24}$/) || inputOrderIdSearch === "") {
            setCondition(inputOrderIdSearch);
            setPage(1);
        }
        else {
            enqueueSnackbar('ID không đúng format', { variant: "error" });
        }
    }
    //khai báo url
    const URL = "http://localhost:8000/Customers/" + userId + "/Orders?orderid=" + condition

    //khai báo fetch
    const fetchApi = async (urlApi, bodyApi) => {
        const response = await fetch(urlApi, bodyApi);
        const data = await response.json();
        return data;
    }

    //set page pagination
    const [noPage, setNoPage] = useState(0);
    const [page, setPage] = useState(1);
    const onPageChange = (event, value) => {
        setPage(value);
    }

    useEffect(() => {
        fetchApi("http://localhost:8000/Customers/" + userId)
            .then((data) => {
                setFullName(data.data.fullName);
                setEmail(data.data.email);
                setPhone(data.data.phone);
                setAddress(data.data.address);
                setCity(data.data.city);
                setCountry(data.data.country);
            })
            .catch((err) => {
                throw err;
            })
        fetchApi(URL)
            .then((data) => {
                setOrderList(data.data);
                setNoPage(Math.ceil(data.data.length / 5));
            })
            .catch((err) => {
                throw err;
            })
    }, [URL, userId])

    return (
        <div className="s24-backgroundPage">
            <Header />
            <Container sx={{ marginTop: 8, paddingTop: 2 }}>
                <BreadCrumb breadCrumbList={breadCrumbList} />
            </Container>
            <Container sx={{ marginTop: 5 }}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12} md={3}>
                        <h4 className='s24-cl1 mt-1'>CUSTOMER INFO</h4>
                        <Grid container className='p-3 my-4 rounded-3 s24-bg3'>
                            <Paper className='bg-white p-3' sx={{ width: "100%" }}>
                                <Stack spacing={2}>
                                    <TextField
                                        disabled
                                        id="fullName"
                                        name="fullName"
                                        label="Full Name"
                                        fullWidth
                                        variant="standard"
                                        value={fullName}
                                    />
                                    <TextField
                                        disabled
                                        type="email"
                                        id="email"
                                        name="email"
                                        label="Email"
                                        fullWidth
                                        variant="standard"
                                        value={email}
                                    />
                                    <TextField
                                        disabled
                                        type="number"
                                        id="phone"
                                        name="phone"
                                        label="Phone number"
                                        fullWidth
                                        variant="standard"
                                        value={phone}
                                    />
                                    <TextField
                                        disabled
                                        id="address"
                                        name="address"
                                        label="Address"
                                        fullWidth
                                        variant="standard"
                                        value={address}
                                    />
                                    <Stack direction="row" spacing={2}>
                                        <TextField
                                            disabled
                                            id="city"
                                            name="city"
                                            label="City"
                                            fullWidth
                                            variant="standard"
                                            value={city}
                                        />
                                        <TextField
                                            disabled
                                            id="country"
                                            name="country"
                                            label="Country"
                                            fullWidth
                                            variant="standard"
                                            value={country}
                                        />
                                    </Stack>
                                </Stack>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={12} md={9}>
                        <Grid container justifyContent="space-between" alignItems="center">
                            <Grid item>
                                <h4 className='s24-cl1'>ORDER LIST</h4>
                            </Grid>
                            <Grid item>
                                <Paper
                                    component="form"
                                    sx={{ display: 'flex', alignItems: 'center', width: 280 }}
                                >
                                    <InputBase
                                        sx={{ ml: 1, flex: 1 }}
                                        placeholder="Search Order ID....."
                                        value={inputOrderIdSearch} onChange={(event) => setInputOrderIdSearch(event.target.value)}
                                    />
                                    <IconButton sx={{ p: '10px' }} aria-label="search" onClick={onBtnSearchClickHandle}>
                                        <ManageSearchIcon />
                                    </IconButton>
                                </Paper>
                            </Grid>
                        </Grid>
                        <Grid container className='p-3 my-3 rounded-3 s24-bg4'>
                            <Paper className='s24-bg3 p-3' sx={{ width: "100%" }}>
                                <TableContainer component={Paper}>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell />
                                                <TableCell><b>Order ID</b></TableCell>
                                                <TableCell align="center"><b>Order Date</b></TableCell>
                                                <TableCell align="center"><b>Shipped Date</b></TableCell>
                                                <TableCell align="center"><b>Cost</b></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                orderList ?
                                                    orderList
                                                        .slice(5 * (page - 1), 5 * page)
                                                        .map((orderRow, index) => {
                                                            return (
                                                                <RowCollapse key={index} orderRow={orderRow} />
                                                            )
                                                        })
                                                    :
                                                    <TableRow>
                                                        <TableCell colSpan={5}>No Order...</TableCell>
                                                    </TableRow>
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                <Grid sx={{ display: "flex", justifyContent: "flex-end" }}>
                                    <Pagination showFirstButton showLastButton count={noPage} page={page} onChange={onPageChange} />
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
            <Footer />
        </div>
    );
}

export default MyOrder;
