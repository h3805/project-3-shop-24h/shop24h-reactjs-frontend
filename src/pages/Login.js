import { Box, Button, Divider, Grid, TextField, Typography, Link } from "@mui/material";
import GoogleIcon from '@mui/icons-material/Google';
import { auth, googleProvider } from "../firebase"
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import "../App.css";

const Login = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    //khai báo fetch
    const fetchApi = async (urlApi, bodyApi) => {
        const response = await fetch(urlApi, bodyApi);
        const data = await response.json();
        return data;
    }

    const loginGoogle = () => {
        auth.signInWithPopup(googleProvider)
            .then((userData) => {
                fetchApi("http://localhost:8000/Customers")
                    .then((data) => {
                        data.data.forEach(element => {
                            if (element.email === userData.user.email) {
                                dispatch({
                                    type: "ADD_USER_ID",
                                    payload: {
                                        userId: element._id
                                    }
                                });
                            }
                        });
                    })
                    .catch((err) => {
                        throw err;
                    })
                navigate("/");
            })
            .catch((err) => {
                throw err;
            })
    }
    const singupGoogle = () => {
        window.open(
            'https://accounts.google.com/signup/v2/webcreateaccount?continue=https%3A%2F%2Fwww.google.com%2F&hl=vi&dsh=S906895722%3A1650899998061733&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp',
            'Sign Up Google',
            'resizable,height=600,width=500'
        );
        return false;
    }
    return (
        <Grid container className="s24-bg1">
            <Grid item xs={12}>
                <Box sx={{ display: { xs: 'none', sm: 'flex' } }}>
                    <Grid container justifyContent="center" alignItems="center" sx={{ minHeight: '100vh' }}>
                        <Grid item xs={5} >
                            <Grid className="s24-bg2" container direction="column" sx={{ marginX: "auto", marginY: 0, padding: 5, borderRadius: 3 }} >
                                <Grid item xs={12} marginY={2}>
                                    <Button fullWidth size="large" variant="contained" sx={{ backgroundColor: "#eceff3", color: "#4a3a27" }} startIcon={<GoogleIcon />} onClick={loginGoogle}>
                                        Sign in with Google
                                    </Button>
                                </Grid>
                                <Grid item xs={12} marginY={2}>
                                    <Box sx={{ alignItems: 'center', display: 'flex' }} >
                                        <Divider sx={{ flexGrow: 1 }} orientation="horizontal" />
                                        <Button variant="outlined" sx={{ cursor: 'unset', borderRadius: `50%` }} disabled >
                                            OR
                                        </Button>
                                        <Divider sx={{ flexGrow: 1 }} orientation="horizontal" />
                                    </Box>
                                </Grid>
                                <Grid item xs={12} marginY={2}>
                                    <Grid container marginY={1}>
                                        <Grid item xs={12}>
                                            <TextField fullWidth label="Username" variant="outlined" disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container marginY={1}>
                                        <Grid item xs={12}>
                                            <TextField fullWidth label="Password" variant="outlined" type="password" disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container marginY={1}>
                                        <Grid item xs={12}>
                                            <Button fullWidth size="large" variant="contained" sx={{ backgroundColor: "#182748", color: "#e89f62" }} disabled>
                                                Sign in
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid container marginY={3}>
                                <Grid item xs={12} textAlign="center" >
                                    <Typography variant="h6" className="s24-cl3">
                                        Don't have an account? <Link to="#" sx={{ cursor: "pointer", color: "#f47820", textDecoration: "unset" }} onClick={singupGoogle}> Sign up here</Link>
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Box>
                <Box sx={{ display: { xs: 'flex', sm: 'none' } }}>
                    <Grid container justifyContent="center" alignItems="center" sx={{ minHeight: '100vh' }}>
                        <Grid item xs={10} >
                            <Grid className="s24-bg2" container direction="column" sx={{ marginX: "auto", marginY: 0, padding: 5, borderRadius: 3 }} >
                                <Grid item xs={12} marginY={2}>
                                    <Button fullWidth size="large" variant="contained" sx={{ backgroundColor: "#eceff3", color: "#4a3a27" }} startIcon={<GoogleIcon />} onClick={loginGoogle}>
                                        Sign in with Google
                                    </Button>
                                </Grid>
                                <Grid item xs={12} marginY={2}>
                                    <Box sx={{ alignItems: 'center', display: 'flex' }} >
                                        <Divider sx={{ flexGrow: 1 }} orientation="horizontal" />
                                        <Button variant="outlined" sx={{ cursor: 'unset', borderRadius: `50%` }} disabled >
                                            OR
                                        </Button>
                                        <Divider sx={{ flexGrow: 1 }} orientation="horizontal" />
                                    </Box>
                                </Grid>
                                <Grid item xs={12} marginY={2}>
                                    <Grid container marginY={2}>
                                        <Grid item xs={12}>
                                            <TextField fullWidth label="Username" variant="outlined" disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container marginY={2}>
                                        <Grid item xs={12}>
                                            <TextField fullWidth label="Password" variant="outlined" type="password" disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container marginY={2}>
                                        <Grid item xs={12}>
                                            <Button fullWidth size="large" variant="contained" color="success" disabled>
                                                Sign in
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid container marginY={3}>
                                <Grid item xs={12} textAlign="center" >
                                    <Typography variant="body1" sx={{ color: "#eceff3" }}>
                                        Don't have an account? <Link to="#" sx={{ cursor: "pointer", color: "#f47820", textDecoration: "unset" }} onClick={singupGoogle} > Sign up here</Link>
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Box>
            </Grid>
        </Grid>
    )
}
export default Login;